import * as actionTypes from '../../../shared/actions';

export const setBasering = (action) => {
    return (dispatch) => {
        dispatch(handleActionDispatch(actionTypes.SET_BASERING, action));
    }
};

export const handleActionDispatch = (actionName, data) => {
    return {
        type: actionName,
        data: data
    }
}