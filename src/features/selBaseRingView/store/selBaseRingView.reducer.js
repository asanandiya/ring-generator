import * as actionType from '../../../shared/actions';
import { updateObject } from '../../../shared/utility';

const initialState = {
	selBaseRing: null
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case (actionType.SET_BASERING):
			return setSelectedBasering(state, action);
		default:
			return state;
	}
};

const setSelectedBasering = (state, action) => {
	return updateObject(state, {
		selBaseRing: action.data
	});
};

export default reducer;
