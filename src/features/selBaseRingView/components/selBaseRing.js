import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import HamBurgerIcon from '@material-ui/icons/Menu';
import Logo from '../../../assets/images/logo.png';
//import SideRing from '../../../assets/images/sidering.png';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import CollectionIcon from '@material-ui/icons/Collections';
import SaveIcon from '@material-ui/icons/Save';
import ReplayIcon from '@material-ui/icons/Replay';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

const images = require.context('../../../assets/images', true);
// const SideRing = images('./sidering.png');
// console.log(SideRing)
class selBaseRing extends Component {

  render() {
    return (
      <Container fixed style={{ width: '100%', height: '100vh', padding: 0, maxWidth: '100%', backgroundColor: '#fff',  overflow: 'hidden' }}>
        {/* Top Header */}
        <AppBar style={{ backgroundColor: '#424242' }} position="static">
          <Toolbar>
            <IconButton onClick={() => this.props.onOpenMenu()} edge="start" color="inherit" aria-label="Menu">
              <HamBurgerIcon />
            </IconButton>
            <Typography variant="body1">
              My Ring
            </Typography>
            <div style={{ flex: 1 }}></div>
            <a style={{ textDecoration: 'none' }} href="#" target="_blank">
              <img src={Logo} style={{ maxHeight: '44px', maxWidth: '25vw' }} />
            </a>
          </Toolbar>
        </AppBar>
        <Grid container direction="column" justify="center" alignItems="center" style={{ backgroundColor: '#fff', height: 'calc(100vh - 112px)', overflow: 'auto'  }}>
          <Grid item xs={4} container direction="column" justify="center" alignItems="center">
            <img src={images(`.${this.props.baseRing}`)} alt="SideRing" style={{ width: '75%' }} />
            <div style={{display:'flex', flexDirection:'row'}}>
            <Typography variant="body1">
              You have 12 mm left.&nbsp;&nbsp;
              </Typography>
            <Typography onClick={() => this.props.onBack()} variant="body1" style={{ cursor: 'pointer', color: '#007bff' }}>
              Change basering?
              </Typography>
              </div>
              <Tooltip title="Add a ring" placement="top">
              <Fab onClick={() => this.props.onAddRing()} style={{ color: '#fff;', backgroundColor: '#424242', marginTop: '2rem', marginBottom: '1rem' }}>
                <AddIcon style={{ color: '#fff' }} />
              </Fab>
            </Tooltip>
          </Grid>         
        </Grid>
        {/* Footer */}
        <Grid container justify="center" alignItems="center" style={{ width: '100%', height: '48px', backgroundColor: '#212121' }}>
          <Tooltip title="Back" placement="top">
            <Button onClick={() => this.props.onBack()} variant="contained" style={{ width: '25%', backgroundColor: '#212121', color: '#fff' }}>
              <ArrowBackIcon />
            </Button>
          </Tooltip>
          <Tooltip title="View your configuration" placement="top">
            <Button variant="contained" style={{ width: '25%', backgroundColor: '#212121', color: '#fff' }}>
              <CollectionIcon />
            </Button>
          </Tooltip>
          <Tooltip title="Save your iXXXi ring" placement="top">
            <Button variant="contained" style={{ width: '25%', backgroundColor: '#212121', color: '#fff' }}>
              <SaveIcon />
            </Button>
          </Tooltip>
          <Tooltip title="Start over" placement="top">
            <Button variant="contained" style={{ width: '25%', backgroundColor: '#212121', color: '#fff' }}>
              <ReplayIcon />
            </Button>
          </Tooltip>
        </Grid>
      </Container>
    );
  }

}

export default withRouter(selBaseRing);
