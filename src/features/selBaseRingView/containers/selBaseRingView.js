import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import SelBaseRing from '../components/selBaseRing';
import * as menuActions from '../../../components/SideMenu/store/sideMenu.action'

class SelBaseRingView extends Component {
    // state = {
    // }

    // componentDidMount() {

    // }

	onAddRing = () => {
        this.props.history.push('/add-rings')
    }

    onOpenMenu = () => {
        this.props.onOpenMenu()
    }

    back = () =>{
        this.props.history.push("base-rings")
    }
    render() {
        return (<Fragment>
            <SelBaseRing
            baseRing={this.props.selBaseRing}
            onBack={this.back}
            onOpenMenu={this.onOpenMenu}
			onAddRing={this.onAddRing}
            />
        </Fragment>)
    }

}

const mapStateToProps = (state) => {
    return {
        selBaseRing:state.baseRing.selBaseRing
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onOpenMenu: () => dispatch(menuActions.open())
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(SelBaseRingView);
