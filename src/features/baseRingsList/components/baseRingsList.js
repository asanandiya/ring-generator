import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import HamBurgerIcon from '@material-ui/icons/Menu';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import imageData from '../../../assets/data/baseImages.json';
const images = require.context('../../../assets/images', true);
// import Image1 from '../../../assets/images/1.jpg';
// import Image2 from '../../../assets/images/2.jpg';
// import Image3 from '../../../assets/images/3.jpg';
// import Image4 from '../../../assets/images/4.jpg';
// import Image5 from '../../../assets/images/5.jpg';
import Logo from '../../../assets/images/logo.png';
import Tooltip from '@material-ui/core/Tooltip';

// const tileData = [
// 	{
// 		img: Image1,
// 		title: 'R01701-01 (8mm)',
// 		id: 1,
// 	},
// 	{
// 		img: Image2,
// 		title: 'R01701-01 (8mm)',
// 		id: 2,
// 	},
// 	{
// 		img: Image3,
// 		title: 'R01701-01 (8mm)',
// 		id: 3,
// 	},
// 	{
// 		img: Image4,
// 		title: 'R01701-01 (8mm)',
// 		id: 4,
// 	},
// 	{
// 		img: Image5,
// 		title: 'R01701-01 (8mm)',
// 		id: 5,
// 	},
// 	{
// 		img: Image1,
// 		title: 'R01701-01 (8mm)',
// 		id: 6,
// 	},
// 	{
// 		img: Image2,
// 		title: 'R01701-01 (8mm)',
// 		id: 7,
// 	},
// 	{
// 		img: Image3,
// 		title: 'R01701-01 (8mm)',
// 		id: 8,
// 	},
// 	{
// 		img: Image4,
// 		title: 'R01701-01 (8mm)',
// 		id: 9,
// 	},
// 	{
// 		img: Image5,
// 		title: 'R01701-01 (8mm)',
// 		id: 10,
// 	},
// 	{
// 		img: Image1,
// 		title: 'R01701-01 (8mm)',
// 		id: 11,
// 	},
// 	{
// 		img: Image2,
// 		title: 'R01701-01 (8mm)',
// 		id: 12,
// 	},
// 	{
// 		img: Image3,
// 		title: 'R01701-01 (8mm)',
// 		id: 13,
// 	},
// 	{
// 		img: Image4,
// 		title: 'R01701-01 (8mm)',
// 		id: 14,
// 	},
// 	{
// 		img: Image5,
// 		title: 'R01701-01 (8mm)',
// 		id: 15,
// 	},
// 	{
// 		img: Image1,
// 		title: 'R01701-01 (8mm)',
// 		id: 16,
// 	},
// 	{
// 		img: Image2,
// 		title: 'R01701-01 (8mm)',
// 		id: 17,
// 	},
// 	{
// 		img: Image3,
// 		title: 'R01701-01 (8mm)',
// 		id: 18,
// 	},
// 	{
// 		img: Image4,
// 		title: 'R01701-01 (8mm)',
// 		id: 19,
// 	},
// 	{
// 		img: Image5,
// 		title: 'R01701-01 (8mm)',
// 		id: 20,
// 	},
// ];

class baseRingsList extends Component {

	render() {

		return (
			<Container fixed style={{ width: '100%', height: '100vh', padding: 0, maxWidth: '100%', backgroundColor: '#fff', overflow: 'hidden' }}>
				{/* Top Header */}
				<AppBar style={{ backgroundColor: '#424242' }} position="static">
					<Toolbar>
						<IconButton onClick={() => this.props.onOpenMenu()} edge="start" color="inherit" aria-label="Menu">
							<HamBurgerIcon />
						</IconButton>
						<Typography variant="body1">
							Choose a basering
          				</Typography>
						<div style={{ flex: 1 }}></div>
						<a style={{ textDecoration: 'none' }} href="#" target="_blank">
							<img src={Logo} style={{ maxHeight: '44px', maxWidth: '25vw' }} />
						</a>
					</Toolbar>
				</AppBar>
				<Grid container style={{ overflow: 'hidden', display: 'flex', flexWrap: 'wrap', backgroundColor: '#fff', height: 'calc(100vh - 112px)', overflow: 'auto' }}>
					{imageData.map(tile => (
						<Grid item key={tile.id} onClick={() => this.props.onSelectBaseRing(tile.url)} xs={2} style={{ padding: '0 15px', marginBottom: '1rem', cursor: 'pointer' }}>
							<img src={images(`.${tile.url}`)} alt={tile.title} style={{ top: '50%', width: '100%' }} />
							<Typography variant="body1" color="textSecondary" style={{ textAlign: 'center' }}
							>
								{tile.title}
							</Typography>
						</Grid>
					))}
				</Grid>
				{/* Footer */}
				<Grid container justify="center" align="center" style={{ width: '100%', height: '48px', backgroundColor: '#212121' }}>
					{/* <Tooltip title="Back" placement="top">
						<Button variant="contained" style={{ width: '100%', backgroundColor: '#212121', color: '#fff' }}>
							<ArrowBackIcon />
						</Button>
					</Tooltip> */}
				</Grid>
			</Container>
		);
	}

}

export default withRouter(baseRingsList);
