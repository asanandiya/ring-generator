import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import BaseRingsList from '../components/baseRingsList';
import * as menuActions from '../../../components/SideMenu/store/sideMenu.action';
import * as selBaseRingActions from '../../../features/selBaseRingView/store/selBaseRingView.action';
class BaseRingsListCon extends Component {

    onSelectBaseRing = (url) => {
        this.props.setSelectedBaseRing(url)
        this.props.history.push("sel-basering")
    }

    onOpenMenu = () => {
        this.props.onOpenMenu()
    }

    render() {
        return (<Fragment>
            <BaseRingsList
                onSelectBaseRing={this.onSelectBaseRing}
                onOpenMenu={this.onOpenMenu}
            />
        </Fragment>)
    }

}

const mapStateToProps = (state) => {
    return {
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onOpenMenu: () => dispatch(menuActions.open()),
        setSelectedBaseRing: (url) => dispatch(selBaseRingActions.setBasering(url))
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(BaseRingsListCon);