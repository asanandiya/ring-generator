import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import HamBurgerIcon from '@material-ui/icons/Menu';
import Logo from '../../../assets/images/logo.png';
import InfoIcon from '@material-ui/icons/Info';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import BaseRingImg from '../../../assets/images/addring.png';
import Image6 from '../../../assets/images/6.jpg';
import Image7 from '../../../assets/images/7.jpg';
import Image8 from '../../../assets/images/8.jpg';
import Image9 from '../../../assets/images/9.jpg';
import Image10 from '../../../assets/images/10.jpg';

const tileData = [
    {
        img: Image6,
        title: 'R01701',
        id: 1,
    },
    {
        img: Image7,
        title: 'R01701',
        id: 2,
    },
    {
        img: Image8,
        title: 'R01701',
        id: 3,
    },
    {
        img: Image9,
        title: 'R01701',
        id: 4,
    },
    {
        img: Image10,
        title: 'R01701',
        id: 5,
    },
    {
        img: Image6,
        title: 'R01701',
        id: 6,
    },
    {
        img: Image7,
        title: 'R01701',
        id: 7,
    },
    {
        img: Image8,
        title: 'R01701',
        id: 8,
    },
    {
        img: Image9,
        title: 'R01701',
        id: 9,
    },
    {
        img: Image10,
        title: 'R01701',
        id: 10,
    },
    {
        img: Image6,
        title: 'R01701',
        id: 11,
    },
    {
        img: Image7,
        title: 'R01701',
        id: 12,
    },
    {
        img: Image8,
        title: 'R01701',
        id: 13,
    },
    {
        img: Image9,
        title: 'R01701',
        id: 14,
    },
    {
        img: Image10,
        title: 'R01701',
        id: 15,
    },
    {
        img: Image6,
        title: 'R01701',
        id: 16,
    },
    {
        img: Image7,
        title: 'R01701',
        id: 17,
    },
    {
        img: Image8,
        title: 'R01701',
        id: 18,
    },
    {
        img: Image9,
        title: 'R01701',
        id: 19,
    },
    {
        img: Image10,
        title: 'R01701',
        id: 20,
    },
    {
        img: Image6,
        title: 'R01701',
        id: 21,
    },
    {
        img: Image7,
        title: 'R01701',
        id: 22,
    },
    {
        img: Image8,
        title: 'R01701',
        id: 23,
    },
    {
        img: Image9,
        title: 'R01701',
        id: 24,
    },
    {
        img: Image10,
        title: 'R01701',
        id: 25,
    },
    {
        img: Image6,
        title: 'R01701',
        id: 26,
    },
    {
        img: Image7,
        title: 'R01701',
        id: 27,
    },
    {
        img: Image8,
        title: 'R01701',
        id: 28,
    },
    {
        img: Image9,
        title: 'R01701',
        id: 29,
    },
    {
        img: Image10,
        title: 'R01701',
        id: 30,
    },
];


class addRingList extends Component {

    render() {
        return (
            <Container fixed style={{ width: '100%', height: '100vh', padding: 0, maxWidth: '100%', backgroundColor: '#fff', overflow: 'hidden' }}>
                {/* Top Header */}
                <AppBar style={{ backgroundColor: '#424242' }} position="static">
                    <Toolbar>
                        <IconButton onClick={() => this.props.onOpenMenu()} edge="start" color="inherit" aria-label="Menu">
                            <HamBurgerIcon />
                        </IconButton>
                        <Typography variant="body1">
                            Add a ring
                        </Typography>
                        <div style={{ flex: 1 }}></div>
                        <a style={{ textDecoration: 'none' }} href="#" target="_blank">
                            <img src={Logo} alt="Logo" style={{ maxHeight: '44px', maxWidth: '25vw' }} />
                        </a>
                    </Toolbar>
                </AppBar>
                <Grid container direction="column" style={{ backgroundColor: '#fff', height: 'calc(100vh - 112px)', overflow: 'auto'  }}>
                    <div style={{  width:'100%', height: '64px', backgroundColor: '#f5f5f5', color: '#333', padding: '16px 24px', display:'flex', flexDirection:'row', justifyContent:'center', alignItems:'center' }}>
                        <InfoIcon />
                        <Typography variant="body2">
                            &nbsp;&nbsp;You have 10 mm left.
                        </Typography>
                        <div style={{ flex: 1 }}></div>
                        <img src={BaseRingImg} style={{ maxWidth: '5vw', height: 'auto' }} />
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', maxWidth: '100vw', margin: '1rem 0 1rem 0', overflow:'hidden', height:'calc(100vh - 208px)' }}>
                        <div style={{ width: 425, display: 'block', position: 'relative', padding: 16, overflow: 'auto' }}>
                            <Grid item style={{ marginBottom: 8 }}>
                                <Typography variant="h5" style={{ textAlign: 'center' }}>
                                    Zirconia 2 mm
						        </Typography>
                            </Grid>
                            <Grid container style={{ display: 'flex', flexWrap: 'wrap', backgroundColor: '#fff' }}>
                                {tileData.map(tile => (
                                    <Grid item key={tile.id} onClick={() => this.props.onSelectBaseRing()} xs={3} style={{ padding: '0 8px', marginBottom: '1rem', cursor: 'pointer' }}>
                                        <img src={tile.img} alt={tile.title} style={{ top: '50%', width: '100%' }} />
                                        <Typography variant="caption" color="textSecondary" style={{ textAlign: 'center', display: 'block' }}
                                        >
                                            {tile.title}
                                        </Typography>
                                    </Grid>
                                ))}
                            </Grid>
                        </div>
                        <div style={{ width: 425, display: 'block', position: 'relative', padding: 16, overflow: 'auto' }}>
                            <Grid item style={{ marginBottom: 8 }}>
                                <Typography variant="h5" style={{ textAlign: 'center' }}>
                                    1 mm
						        </Typography>
                            </Grid>
                            <Grid container style={{ maxHeight: 450, display: 'flex', flexWrap: 'wrap', backgroundColor: '#fff' }}>
                                {tileData.map(tile => (
                                    <Grid item key={tile.id} onClick={() => this.props.onSelectBaseRing()} xs={3} style={{ padding: '0 8px', marginBottom: '1rem', cursor: 'pointer' }}>
                                        <img src={tile.img} alt={tile.title} style={{ top: '50%', width: '100%' }} />
                                        <Typography variant="caption" color="textSecondary" style={{ textAlign: 'center', display: 'block' }}
                                        >
                                            {tile.title}
                                        </Typography>
                                    </Grid>
                                ))}
                            </Grid>
                        </div>
                        <div style={{ width: 425, display: 'block', position: 'relative', padding: 16, overflow: 'auto' }}>
                            <Grid item style={{ marginBottom: 8 }}>
                                <Typography variant="h5" style={{ textAlign: 'center' }}>
                                    2 mm
						        </Typography>
                            </Grid>
                            <Grid container style={{ maxHeight: 450, display: 'flex', flexWrap: 'wrap', backgroundColor: '#fff' }}>
                                {tileData.map(tile => (
                                    <Grid item key={tile.id} onClick={() => this.props.onSelectBaseRing()} xs={3} style={{ padding: '0 8px', marginBottom: '1rem', cursor: 'pointer' }}>
                                        <img src={tile.img} alt={tile.title} style={{ top: '50%', width: '100%' }} />
                                        <Typography variant="caption" color="textSecondary" style={{ textAlign: 'center', display: 'block' }}
                                        >
                                            {tile.title}
                                        </Typography>
                                    </Grid>
                                ))}
                            </Grid>
                        </div>
                    </div>
                </Grid>
                {/* Footer */}
                <Grid container justify="center" align="center" style={{ width: '100%', height: '48px', backgroundColor: '#212121' }}>
                    <Tooltip title="Back" placement="top">
                        <Button onClick={() => this.props.onBack()} variant="contained" style={{ width: '100%', backgroundColor: '#212121', color: '#fff' }}>
                            <ArrowBackIcon />
                        </Button>
                    </Tooltip>
                </Grid>
            </Container >
        );
    }

}

export default withRouter(addRingList);
