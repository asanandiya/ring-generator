import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import AddRingList from '../components/addRingList';
import * as menuActions from '../../../components/SideMenu/store/sideMenu.action'
class AddRingListCon extends Component {
    // state = {
    // }

    // componentDidMount() {

    // }
    onOpenMenu = () => {
        this.props.onOpenMenu()
    }

    back = () =>{
        this.props.history.push("sel-basering")
    }

    render() {
        return (<Fragment>
            <AddRingList 
            onBack={this.back}
            onOpenMenu={this.onOpenMenu}
            />
        </Fragment>)
    }

}

const mapStateToProps = (state) => {
    return {
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onOpenMenu: () => dispatch(menuActions.open())
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(AddRingListCon);
