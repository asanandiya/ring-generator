import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { BrowserRouter } from 'react-router-dom';
import menuReducer from './components/SideMenu/store/sideMenu.reducer'
import baseRingReducer from './features/selBaseRingView/store/selBaseRingView.reducer'
import App from './App';
import './index.css';

// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// );
let reducers = {
	menu: menuReducer,
	baseRing: baseRingReducer
}
const rootReducer = combineReducers(reducers);
const store = createStore(rootReducer, compose(applyMiddleware(thunk)));
class Root extends Component {
	render() {
		let children;
		children = (
			<Provider store={store}>
				<BrowserRouter>
					{this.props.children}
				</BrowserRouter>
			</Provider>
		);
		return children
	}
}
const render = Component => {
	return ReactDOM.render(<Root style={{ height: '100%', width: '100%', overflow: 'hidden' }}><Component /></Root>,
		document.getElementById('root')
	);
};
render(App);
