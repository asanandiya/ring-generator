import React, { Component } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import BaseRingsList from './features/baseRingsList/containers/baseRingsList';
import SelBaseRingView from './features/selBaseRingView/containers/selBaseRingView';
import AddRingList from './features/addRingList/containers/addRingList';
import SideMenu from './components/SideMenu/SideMenu'
import * as menuActions from './components/SideMenu/store/sideMenu.action'
import CssBaseline from '@material-ui/core/CssBaseline';

import './App.css';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <SideMenu 
        isOpen={this.props.isMenuOpen}
        closeMenu={this.props.closeMenu}
        />
        {this.getRoutes()}
      </React.Fragment>
    );
    // return (
    //   <div className="App">
    //     <div className="App-header">
    //       <img src={logo} className="App-logo" alt="logo" />
    //       <h2>Welcome to React</h2>
    //     </div>
    //     <p className="App-intro">
    //       To get started, edit <code>src/App.js</code> and save to reload.
    //     </p>
    //   </div>
    // );
  }


  getRoutes() {
    return (
      <React.Fragment>
        <Switch>
          <Route path="/add-rings" exact component={AddRingList} />
          <Route path="/base-rings" exact component={BaseRingsList} />
          <Route path="/sel-basering" exact component={SelBaseRingView} />
          <Redirect to="/base-rings" />
        </Switch>
      </React.Fragment>
    )

  }
}

const mapStateToProps = state => {
  return {
    isMenuOpen: state.menu.open
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    closeMenu: () => dispatch(menuActions.close())
  }

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
