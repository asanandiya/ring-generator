import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import classes from './SideMenu.module.css';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseButton from '@material-ui/icons/Close';

const menu = [{
    name: "Ring Generator",
    route: "base-rings"
}, {
    name: "Saved Rings",
    route: "base-rings",
    divider: true
}, {
    name: "Store Locator",
    route: "base-rings",
    divider: true
}]
class SideMenu extends Component {
    constructor(props) {
        super(props);
    };
    closeMenu = e => {
        this.props.closeMenu();
    }

    onMenu = (e, item) => {
        this.props.history.push(item.route);
        this.closeMenu()
    }

    render() {
        const {
            onMenu,
            closeMenu
        } = this;
        return (
            <Drawer open={this.props.isOpen}>
                <div style={{width:250}}>
                    <div style={{display:'flex', flexDirection:'row',height: 48, justify:'space-between', alignItems:'center', padding:'0 16px'}}>
                        <Typography variant="body1" color="textSecondary">
                            Menu
                        </Typography>
                        <div style={{flex:1}}></div>
                        <IconButton size="small" onClick={(event) => closeMenu(event)}>
                            <CloseButton fontSize="inherit" />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        {menu.map((item, index) => (
                            <ListItem onClick={(event) => onMenu(event, item)} button key={item.name}>                                
                                <ListItemText style={{fontSize:'0.9rem'}} primary={item.name} />                                
                                {item.divider ? <Divider /> : null}
                            </ListItem>
                        ))}
                    </List>
                </div>
            </Drawer>
        )
    }
}

export default withRouter(SideMenu);