import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import BaseRingsList from '../components/baseRingsList';
import * as menuActions from '../../../components/SideMenu/store/sideMenu.action'

class SideMenuCon extends Component {
    
    onSelectBaseRing = () => {
        this.props.history.push('/sel-basering')
    }

    onOpenMenu = () => {
        this.props.onOpenMenu()
    }

    render() {
        return (<Fragment>
            <BaseRingsList
                onSelectBaseRing={this.onSelectBaseRing}
                onOpenMenu={this.onOpenMenu}
            />
        </Fragment>)
    }

}

const mapStateToProps = (state) => {
    return {
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onOpenMenu: () => dispatch(menuActions.open())
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(BaseRingsListCon);