import * as actionType from '../../../shared/actions';
import { updateObject } from '../../../shared/utility';

const initialState = {
	open: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case (actionType.OPEN_MENU):
			return openMenu(state, action);
		case (actionType.CLOSE_MENU):
			return closeMenu(state, action);
		default:
			return state;
	}
};

const openMenu = (state, action) => {
	return updateObject(state, {
		open: true
	});
};

const closeMenu = (state, action) => {
	return updateObject(state, {
		open: false
	});
};

export default reducer;
