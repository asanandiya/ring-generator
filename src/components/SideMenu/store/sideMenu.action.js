import * as actionTypes from '../../../shared/actions';
// import configUrl from '../../../shared/configUrl';

export const open = (action) => {
    return (dispatch) => {
        dispatch(handleActionDispatch(actionTypes.OPEN_MENU, action));
    }
};

export const close = (action) => {
    return (dispatch) => {
        dispatch(handleActionDispatch(actionTypes.CLOSE_MENU, action));
    }
};

export const handleActionDispatch = (actionName, data) => {
    return {
        type: actionName,
        data: data
    }
}