import queryStringParser from 'query-string';

export const updateObject = (oldObject, updatedProperties) => {
	return {
		...oldObject,
		...updatedProperties
	}
};

/* stringify object contains params and it's value into string (In URL format) */
export const queryStringify = (queryParams) => {
	return queryStringParser.stringify(queryParams);
};
